import React from 'react';
import StatefulButton from '../StatefulButton/StatefulButton'
import { EMPTY } from 'rxjs';
import { tap, delay, map } from 'rxjs/operators';
import './XstatePlayground.css';


interface Props { }
interface State {
  screenshotUrl: string
}

class XstatePlayground extends React.Component<Props, State>  {

  private readonly successfulTask = () =>
    EMPTY.pipe(
      tap(() => console.log('Kicking off long running task.')),
      delay(1500),
      map(() => ({}))
    ).toPromise();

  private readonly unsuccessfulTask = () =>
    this.successfulTask().then(() => {
      throw new Error('An error occured');
    })

  constructor(props: Props) {
    super(props);

    this.state = {
      screenshotUrl: this.getScreenshotNameForState('active')
    }
  }

  private stateChangeHandler(state: string) {
    this.setState({
      screenshotUrl: this.getScreenshotNameForState(state)
    });
  }

  private getScreenshotNameForState(state: string): string {
    return `/images/xstate-visualizer_${state}.png`;
  }

  render() {
    return (<div className="App">
      <header className="App-header">
        <img id="screenshot"
          src={this.state.screenshotUrl}
          alt={this.state.screenshotUrl} />

        <div>
          A successful log-running task
          <StatefulButton
            displayText="Run"
            handleClick={this.successfulTask}
            handleStateChange={this.stateChangeHandler.bind(this)}
          ></StatefulButton>
        </div>

        <div>
          A failing long-running task
          <StatefulButton
            displayText="Run"
            handleClick={this.unsuccessfulTask}
            handleStateChange={this.stateChangeHandler.bind(this)}
          ></StatefulButton>
        </div>

        <p>
          <a
            className="App-link"
            href="https://xstate.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn XState.js
          </a>
        </p>
      </header>
    </div >);
  }
}

export default XstatePlayground;