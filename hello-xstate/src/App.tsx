import React from 'react';
import './App.css';
import './MozillaButtons.css';

import XstatePlayground from './XstatePlayground/XstatePlayground';


const App: React.FC = () => {
  return <XstatePlayground></XstatePlayground>;
}

export default App;
