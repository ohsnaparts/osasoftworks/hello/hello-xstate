import React, { Props, Component } from 'react';
import { Machine, interpret, EventObject, DefaultContext, Interpreter } from 'xstate';

interface StatefulButtonProps extends Props<StatefulButton> {
  displayText: string,
  handleClick: (event: React.MouseEvent<HTMLElement>) => Promise<{}>;
  handleStateChange: (state: string) => void;
}

interface StatefulButtonState {
  displayText: string,
  currentState: string
}


class StatefulButton extends Component<StatefulButtonProps, StatefulButtonState> {
  private stateMachine: Interpreter<DefaultContext, any, EventObject>;

  constructor(props: StatefulButtonProps) {
    super(props);

    this.state = {
      displayText: this.getButtonContent(this.props.displayText, 'active'),
      currentState: 'active'
    }

    this.stateMachine = interpret(Machine({
      id: 'buttonStates',
      initial: `${this.state.currentState}`,
      states: {
        active: {
          on: { process: 'processing' }
        },
        processing: {
          on: {
            finished: 'active',
            error: 'error'
          }
        },
        error: {
          on: {
            resolve: 'active'
          }
        }
      }
    }))
      .onTransition((nextState: any) => {
        console.log(`Switching states: ${this.state.currentState} -> ${nextState.value}`);
        this.props.handleStateChange(nextState.value);

        this.setState({
          currentState: nextState.value,
          displayText: this.getButtonContent(this.props.displayText, nextState.value)
        });

      });
  }

  public componentDidMount() {
    console.log('Component mounted.');
    console.log('Starting StateMachine.');
    this.stateMachine.start();
  }

  private getButtonContent = (displayText: string, state: string): string =>
    `${displayText} (State:${state})`;


  private buttonClicked = (event: React.MouseEvent<HTMLElement>) => {
    console.log(`Invoking event handler.`);
    this.stateMachine.send('process');

    this.props.handleClick(event).then(() =>
      this.stateMachine.send('finished')
    ).catch(reason =>
      this.stateMachine.send('error')
    );
  }

  private resolveErrorClicked = (event: React.MouseEvent<HTMLElement>) => {
    console.log('Resolving error');
    this.stateMachine.send('resolve');
  }




  render(): any {
    return (<div>
      <button
        className="site"
        onClick={this.buttonClicked.bind(this)}
        disabled={this.state.currentState !== 'active'}>
        {this.state.displayText}
      </button>

      {
        this.state.currentState === 'error' ?
          <button className="site" onClick={this.resolveErrorClicked}>Resolve</button>
          : undefined
      }
    </div>
    );
  }
}

export default StatefulButton;