# Hello-Xstate

A minimal React page for playing with https://xstate.js.org

```bash
npm install
npm start
```

![](./hello-xstate.gif)